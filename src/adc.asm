;   ADC - View the analog values using LED's
;   Copyright (C) 2009  Steven Rodriguez
;
;   This program is part of ADC
;
;   This program is free software: you can redistribute it and/or modify
;   it under the terms of the GNU General Public License as published by
;   the Free Software Foundation, either version 3 of the License, or
;   (at your option) any later version.
;
;   This program is distributed in the hope that it will be useful,
;   but WITHOUT ANY WARRANTY; without even the implied warranty of
;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;   GNU General Public License for more details.
;
;   You should have received a copy of the GNU General Public License
;   along with this program.  If not, see <http://www.gnu.org/licenses/>.
;
;   If this program has problems please contact me at:
;
;   stevencrc@digitecnology.zapto.org

;==============================================
;ADC (2009 - Steven Rodriguez)
;==============================================

;General description
;=============================
; The display leds are connected to:
; - RB0 - 33 - bit 0 of the ADC value
; - RB1 - 34 - bit 1 of the ADC value
; - RB2 - 35 - bit 2 of the ADC value
; - RB3 - 36 - bit 3 of the ADC value
; - RB4 - 37 - bit 4 of the ADC value
; - RB5 - 38 - bit 5 of the ADC value
; - RB6 - 39 - bit 6 of the ADC value
; - RB7 - 40 - bit 7 of the ADC value
; - RD0 - 19 - bit 8 of the ADC value
; - RD1 - 20 - bit 9 of the ADC value
; - RA0 - 2 - analog input
;=============================

;Variables
;==============================
TEMP EQU 0x27

;Assembler directives
;==============================
	list p=16f874a
        include p16f874a.inc
        __config b'11111100111001'
        org 0x00 ;Reset vector
	pagesel Start
        goto Start

;Functions
;==============================
	include digitecnology.inc
ShowADCValue
	pagesel chgbnk1
	call chgbnk1
	movf ADRESL,0
	pagesel chgbnk0
	call chgbnk0
	movwf PORTB
	movf ADRESH,0
	andlw b'00000011'
	movwf TEMP
	movf PORTD,0
	andlw b'11111100'
	iorwf TEMP,0
	movwf PORTD	
	return
;Program
;==============================
Start ;Main program
	pagesel Initialize
	goto Initialize
Initialize
	;Change to bank 0
	pagesel chgbnk0
	call chgbnk0
	;Clear PORTA to PORTD
	clrf PORTA
	clrf PORTB
	clrf PORTC
	clrf PORTD	
	;Change to bank 1
	pagesel chgbnk1
	call chgbnk1
	;Set inputs/outputs
	clrf TRISB ;Outputs
	clrf TRISD ;Outputs
	bsf PORTA,0 ;Input
	;Deactivate analog comparators
	bsf CMCON,2
	bsf CMCON,1
	bsf CMCON,0
	;Configure analog-digital converter
	pagesel chgbnk0
	call chgbnk0
	movlw b'00000001'
	movwf ADCON0
	pagesel chgbnk1
	call chgbnk1
	movlw b'10001110'
	movwf ADCON1
	pagesel chgbnk0
	call chgbnk0		
Cycle
	;Obtain analog-digital value and show it
	bsf ADCON0,2
	pagesel $+1
	btfsc ADCON0,2
	goto $-1
	;Show value
	pagesel ShowADCValue
	call ShowADCValue
	pagesel Cycle
	goto Cycle
	end

